import socket
import read_json
import elso

end_text = "end!"
config = read_json.read_json()
alg = config["blum"]["alg"]
seed = config["blum"]["seed"]
print(seed)

port = 12345
SERVER = socket.gethostbyname(socket.gethostname())
addr = (SERVER, port)


client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(addr)


while True:
    type = input("do you want to send or receive(s/r)")
    if (type == "s"):
        msg,seed = elso.stream_encrypter("s",alg,seed,"E")
        # print(msg)
        client.send(bytes(msg,"utf-8"))
        exit = input("do you want to end conversation(y/n)")
        if (exit == "y"):
            msg,seed = elso.stream_encrypter(end_text,alg,seed,"E")
            print(msg)
            client.send(bytes(msg,"utf-8"))
            client.close()
            break
        else:
            msg = input("we are sending data:")
            msg,seed = elso.stream_encrypter(msg,alg,seed,"E")
            client.send(bytes(msg,"utf-8"))
    else:
        msg,seed = elso.stream_encrypter("r",alg,seed,"E")
        print(msg)
        client.send(bytes(msg,"utf-8"))
        msg = client.recv(54)
        text,seed = elso.stream_encrypter(msg.decode('utf-8'),alg,seed,"D")
        print(f"i got data from {addr}, data: {text}")
    