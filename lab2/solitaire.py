# import random 
# white_joker=53;
# black_joker=54;


def white_joker(cards):
    # print ("white_joker")
    # print(cards)
    position_white_joker = cards.index(53)
    if (position_white_joker == 53):
        cards.insert(0, cards.pop())
        return cards
    x= position_white_joker
    y= position_white_joker + 1
    cards[x], cards[y] = cards[y], cards[x] 
    # print(cards)
    # print("-----------------")
    return cards

# 1,2,3,4,5,5,6,5,3,4,34,34
#x,y,z = y,z,x


def black_joker(cards):
    # print ("black_joker")
    position_black_joker = cards.index(54)
    if (position_black_joker == 53):
        cards.insert(1,cards.pop())
        return cards

    if (position_black_joker == 52):
        temp = cards.pop()
        cards.insert(0,cards.pop())
        cards.append(temp)
        return cards
    x = position_black_joker
    cards[x], cards[x + 1], cards[x + 2] = cards[x+1], cards[x + 2], cards[x]
    # print(cards)
    # print("-----------------")
    return cards

def swap_before_end(cards):
    # print("swap_before_end")
    elso = cards.index(53)
    masodik = cards.index(54)
    first = min(elso, masodik)
    second = max(elso, masodik)
    newcards = []
    newcards.extend(cards[(second+1):54])
    newcards.extend(cards[first:second+1])
    newcards.extend(cards[0:first])
    # print("-----------------")
    # print(cards)
    return newcards

def last_move(cards):
    # print("last_move")
    temporary = cards.pop()
    if temporary == 53 or temporary == 54:
        cards.append(temporary)
        return cards
    i = temporary
    while i:
        cards.append(cards.pop(0))
        i = i -1
    cards.append(temporary)
    # print(cards)
    # print("-----------------")
    return cards

def get_random_number(cards):
    temporary = cards[0]
    if temporary == 53 or temporary == 54:
        return -1
    return cards[temporary]





def solitaire_algorithm(cards):
    # print("problem solitaire algorithm")
    while True:
        cards = white_joker(cards)
        cards = black_joker(cards)
        cards = swap_before_end(cards)
        cards = last_move(cards)
        # print(cards)
        # print("foprogram")
        random_number = get_random_number(cards)
        # print(cards)

        # print(".ß")
        # print(random_number)
        # print(".ß")
        if random_number > 0:
            break
    return cards, random_number

def encrypt_solitaire(message, cards):
    n = len(message)
    result = ""
    for i in range(n):
        cards, value1 = solitaire_algorithm(cards)
        cards, value2 = solitaire_algorithm(cards)

        key_byte = value1*value2 % 256 
        result = result + (chr(key_byte ^ ord(message[i])))
        
    return result, cards


def decrypt_solitaire(message, key):
    return encrypt_solitaire(message, key)



# def main():
#     cards = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,54]
#     result, temp = encrypt_solitaire("mitortenik itt eisze mukodik",cards)
#     print(result)
#     print(cards)
#     print(temp)

    # cards = [24, 22, 17, 18, 33, 46, 47, 25, 34, 54, 21, 40, 26, 48, 12, 14, 7, 19, 10, 50, 38, 51, 27, 43, 37, 32, 4, 1, 52, 13, 5, 39, 28, 6, 20, 11, 45, 30, 3, 9, 15, 42, 2, 16, 53, 23, 44, 8, 29, 41, 31, 49, 35, 36]
    # result, temp = decrypt_solitaire(result,cards)
    # print("ennnee")
    # print(result)
    # a = "t"
    # b = "s"
    # print(2 ^ 3)

    # print(cards)
    # cards, random_number = solitaire_algorithm(cards)
    # print(cards)
    # print (random_number)

    # proba = [2,3,53,54,6,52,6,9]
    # print(swap_before_end(proba))

    # print(cards[cards.index(2):len(cards)-1])
    # print (cards)
    # temp = cards.pop()
    # print(temp)
    # cards.insert(0,cards.pop())
    # print(cards)
    # cards.append(temp)
    # print(cards)


# main()
