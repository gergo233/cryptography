import json

def read_json():
    with open("config.json", "r") as f:
        return json.load(f)