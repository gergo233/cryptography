import solitaire
import blum


def encrypt_message(message, alg, seed):
    if alg == 'S':
        return solitaire.encrypt_solitaire(message, seed)
    return blum.encrypt_blum(message,seed)


def decrypt_message(message, alg, seed):
    if alg == 'S':
        return solitaire.decrypt_solitaire(message, seed)
    return blum.decrypt_blum(message,seed)

def stream_encrypter(message, alg, seed, action):
    if action == 'E':
        return encrypt_message(message, alg, seed)
    return decrypt_message(message, alg, seed)
