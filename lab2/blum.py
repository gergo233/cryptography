import math

def check_prime(x):

    for i in range(2,math.floor(math.sqrt(x))):
        if x % i == 0:
            return False
    return True

# elemnt%4 = 3 kell legyen 
def get_prime_from(element):
    while True:
        if check_prime(element):
            return(element)
        element = element + 4


# def main():
#     seed = [123,173629]
#     print(seed)
#     xd,mas = encrypt_blum("qefdaqdkdaw", seed)
#     print(xd)
#     eredmeny, seed = decrypt_blum(xd,seed)
#     print(eredmeny)

def get_next(seed):
    return [seed[0]*seed[0] % seed[1], seed[1]]


def encrypt_blum(message, seed):
    n = len(message)
    result = ""
    for i in range(n):
        seed = get_next(seed)
        result = result + (chr((seed[0] % 256) ^ ord(message[i])))
        
    return result, seed


def decrypt_blum(message, seed):
    return encrypt_blum(message, seed)

# main()