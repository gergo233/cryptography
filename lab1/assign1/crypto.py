#!/usr/bin/env python3 -tt
"""
File: crypto.py
---------------
Assignment 1: Cryptography
Course: CS 41
Name: <Agoston Gergely-Vince>
SUNet: <agim1986>

Replace this with a description of the program.
"""
from ast import Bytes
import utils

# Caesar Cipher


def encrypt_caesar(plaintext,binary=False):
    """Encrypt plaintext using a Caesar cipher.

    Add more implementation details here.
    """
    if not binary:
        encryptedText = ""
        for letter in plaintext:
            temp = ord(letter)
            if (temp > 64 and temp < 91):
                temp = temp + 3
                if temp > 90:
                    temp = temp-26
            encryptedText += chr(temp)
    else:
        encryptedText = b''
        for letter in plaintext:
           encryptedText +=  (bytes([(letter + 3)%256]))
        # raise NotImplementedError  # Your implementation here
    return encryptedText


def decrypt_caesar(ciphertext,binary=False):
    """Decrypt a ciphertext using a Caesar cipher.

    Add more implementation details here.
    """
    if not binary:
        decryptedText = ""
        for letter in ciphertext:
            temp = ord(letter)
            if (temp > 64 and temp < 91):
                temp = temp - 3
                if temp < 65:
                    temp = temp+26
            decryptedText += chr(temp)
    else:
        decryptedText=b''
        for letter in ciphertext:
            decryptedText += (bytes([(letter - 3)%256]))
    return decryptedText
    # raise NotImplementedError  # Your implementation here


# Vigenere Cipher

def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.

    Add more implementation details here.
    """
    index = 0
    encryptedText = ""
    for letter in plaintext:
        temp = ord(letter)
        temp = temp + ord(keyword[index]) - 65
        if temp > 90:
            temp = temp - 26
        encryptedText += chr(temp)
        index += 1
        if index == len(keyword):
            index = 0

    # raise NotImplementedError  # Your implementation here
    return encryptedText


def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.

    Add more implementation details here.
    """

    index = 0
    decryptedText = ""
    for letter in ciphertext:
        temp = ord(letter)
        temp = temp - ord(keyword[index]) + 65
        if temp < 65:
            temp = temp + 26
        decryptedText += chr(temp)
        index += 1
        if index == len(keyword):
            index = 0

    # raise NotImplementedError  # Your implementation here
    return decryptedText

# Scytale


def encrypt_scytale(plaintext, circumference):
    encryptedText = []
    for i in range(0, circumference):
        j = i
        while j < len(plaintext):
            encryptedText.append(plaintext[j])
            j = j + circumference

    return "".join(encryptedText)

    #raise NotImplementedError


def decrypt_scytale(plaintext, circumference):
    decryptedText = []
    temp = len(plaintext)//circumference
    for i in range(0, circumference-1):
        j = i
        while j < len(plaintext):
            decryptedText.append(plaintext[j])
            j = j + temp

    return "".join(decryptedText)


def encrypt_railfence(plaintext, num_rails):
    matrix = [[' ' for i in range(len(plaintext))] for j in range(num_rails)]
    down = False
    encryptedText = ""
    x = 0
    y = 0
    for letter in plaintext:
        matrix[x][y] = letter
        if (x == num_rails-1 or x == 0):
            down = not (down)
        y = y+1
        if num_rails!=0:
            if down:
                x = x+1
            else:
                x = x-1

    for i in range(num_rails):
        for j in range(0, len(plaintext)):
            if matrix[i][j] != " ":
                encryptedText += (matrix[i][j])

    return encryptedText


def decrypt_railfence(plaintext, num_rails):
    matrix = [[' ' for i in range(len(plaintext))] for j in range(num_rails)]
    down = False
    decryptedText = ""
    x = 0
    y = 0
    for letter in plaintext:
        matrix[x][y] = "#"
        if (x == num_rails-1 or x == 0):
            down = not (down)
        y = y+1
        if num_rails!=0:
            if down:
                x = x+1
            else:
                x = x-1
    index = 0
    for i in range(num_rails):
        for j in range(len(plaintext)):
            if (matrix[i][j] == "#"):
                matrix[i][j] = plaintext[index]
                index += 1

    x = 0
    y = 0
    down = False
    for letter in plaintext:
        decryptedText += matrix[x][y]
        if (x == num_rails-1 or x == 0):
            down = not (down)
        y = y+1
        if num_rails!=0:
            if down:
                x = x+1
            else:
                x = x-1
    return decryptedText


def generate_private_key(n=8):
    """Generate a private key for use in the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key components
    of the MH Cryptosystem. This consistutes 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        (Note: you can check if a sequence is superincreasing with `utils.is_superincreasing(seq)`)
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q` (you can use utils.coprime)

    You'll need to use the random module for this function, which has been imported already

    Somehow, you'll have to return all of these values out of this function! Can we do that in Python?!

    @param n bitsize of message to send (default 8)
    @type n int

    @return 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    raise NotImplementedError  # Your implementation here


def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one line using a list comprehension

    @param private_key The private key
    @type private_key 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    @return n-tuple public key
    """
    raise NotImplementedError  # Your implementation here


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    1. Separate the message into chunks the size of the public key (in our case, fixed at 8)
    2. For each byte, determine the 8 bits (the `a_i`s) using `utils.byte_to_bits`
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk in the message

    Hint: think about using `zip` at some point

    @param message The message to be encrypted
    @type message bytes
    @param public_key The public key of the desired recipient
    @type public_key n-tuple of ints

    @return list of ints representing encrypted bytes
    """
    raise NotImplementedError  # Your implementation here


def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key

    1. Extract w, q, and r from the private key
    2. Compute s, the modular inverse of r mod q, using the
        Extended Euclidean algorithm (implemented at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum using c' and w to recover the original byte
    5. Reconsitite the encrypted bytes to get the original message back

    @param message Encrypted message chunks
    @type message list of ints
    @param private_key The private key of the recipient
    @type private_key 3-tuple of w, q, and r

    @return bytearray or str of decrypted characters
    """
    raise NotImplementedError  # Your implementation here
