import unittest
import random
NRofBits = 5


def generate_private_key():
    w = [0] * NRofBits
    w[0] = random.randint(1, 10)
    s = w[0]
    for i in range(1, NRofBits):
        w[i] = s + random.randint(1, 100)
        s += w[i]

    if not unittest.szupernovekvo(w):
        return -1

    m = random.randint(s + 1, 2 * s)

    a = 0
    while not unittest.lnko(a, NRofBits):
        a = random.randint(2, m - 1)

    return w, m, a


def create_public_key(private_key):
    (w, m, a) = private_key
    temp = []
    for v in w:
        temp.append(a*v % m)
    return temp


def encrypt_mh(message, public_key):
    result = [0] * len(message)
    for i in range(len(message)):
        bits = unittest.convertToByte(message[i])
        if bits == -1:
            print("rossz karakteretek")
            return -3
        for j in range(0,5):
            # print(bits(i))
            # print(publi(i))
            if bits[j]:
                result[i] +=public_key[j] 

    return result


def decrypt_mh(message, private_key):
    (w, m, a) = private_key
    b = unittest.modinv(m, a)
    n = len(message)
    result =[]

    for i in range(0, n):
        c = b * message[i] % m
        bits = [0] * 5
        for j in range(4, -1, -1):
            if c >= w[j]:
                bits[j] = 1
                c -= w[j]
        result.append(unittest.convertToChar(bits))

    return result




# def main():
#     print(unittest.convertToByte('S'))
#     if unittest.convertToByte('2')==-1:
#         print("xd")
#         print([1,0,0,0,0] and [1,2,3,4,5])
#     print(encrypt_mh("SZIA",[51, 6, 74, 24, 48]))
#     a,b,c = generate_private_key()
#     print("e")
#     print(create_public_key(([3, 5, 9, 20, 40],79,17)))
#     print(decrypt_mh(encrypt_mh("SZIA",[51, 6, 74, 24, 48]),([3, 5, 9, 20, 40],79,17)))

# main()
