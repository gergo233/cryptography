import math
NRofBits = 5

def szupernovekvo(szamok):
    s = 0
    for i in szamok:
        if i <= s:
            return False
        s += i
    return True


def lnko(a, b):
    return math.gcd(a, b) == 1


def modinv(a, m):
    g, x, y = egcd(a, m)
    return x % m


def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)


def convertToByte(c):
    c = ord(c.lower())
    if not 97 <= c <= 122:
        return -1
    c -=97
    out = []
    for i in range(NRofBits):
        out.append(c & 1)
        c >>= 1
    return out[::-1]

def convertToChar(x):
    s = 0
    k = 1
    for i in range(len(x)-1,-1,-1):
        if x[i]:
            s+=k 
        k *=2
    return chr(s+97)

# def main():
#     print(convertToChar([1,0,0,1,0]))

# main()